# pix4speed

`pix4speed` is a tool collection to build and maintain tile pyramids
to be served by [MapServer](https://mapserver.org) as
[WMS](https://www.ogc.org/standard/wms/).

⚠ **This is work in progress**. The current focus is on replacing
the old `dataloader`.

## Build

The `dataloader` is written in [Go](https://go.dev/). To build the Software
you need at least version 1.19 or later. You can get it [here](https://go.dev/dl/).

Dowloading the sources with

```shell
wget https://heptapod.host/intevation/pix4speed/-/archive/main/pix4speed-main.tar.gz
tar xfvz pix4speed-main.tar.gz
mv pix4speed-main pix4speed
cd pix4speed
```

or check them out with

```shell
git clone https://heptapod.host/intevation/pix4speed.git
cd pix4speed
```

Building the `dataloader`:

```shell
go build -o bin/dataloader ./cmd/dataloader
```

If you want to cross-compile on a Microsoft Windows Plattform
to target a GNU/Linux platform you can have to set the environment variables
`GOARCH` and `GOOS` before the build, eg.

```shell
SET GOOS=linux
SET GOARCH=amd64
```

The main pyramid tool is `bin/dataloader`. Place this into your `PATH`.

## Distribute to package registry

### Requirements
You have to [create a personal access token](https://heptapod.host/help/user/profile/personal_access_tokens.md#create-a-personal-access-token).

### Upload the binaries

Upload a tarball containing the binaries by using curl:
```
curl --header "PRIVATE-TOKEN: <your_token> --upload-file <file_name> "https://heptapod.host/api/v4/projects/1540/packages/generic/<package_name>/<package_version>/<file_name>"
```

An example:
```
curl --header "PRIVATE-TOKEN: xxxxxxx" --upload-file dataloader-linux-amd64-2.0.3.tar.gz "https://heptapod.host/api/v4/projects/1540/packages/generic/pix4speed/2.0.3/dataloader-linux-amd64-2.0.3.tar.gz"
```

When the upload was successful curl returns `{"message":"201 Created"}`.

The package can now be downloaded on: https://heptapod.host/intevation/pix4speed/-/packages

You can find more details about this distribution method (e.g. allowed file names) in the [documentation](https://heptapod.host/help/user/packages/generic_packages/index.md#publish-a-package-file).

## External tools

To run the `dataloader` you need some extra tools installed:

* `xmllint` from [Libxml2](https://gitlab.gnome.org/GNOME/libxml2/-/wikis/home) to validate XML files.
* The [GDAL](https://gdal.org/programs/index.html) raster programs
  * `gdalinfo`
  * `gdalbuildvrt`
  * `gdal_translate`
  * `gdaladdo`
* `shptree` from [MapServer](https://mapserver.org/utilities/shptree.html)
  to create spatial indices for shapefiles.

To install it on OpenSUSE Leap 15.4 use:

```shell
sudo zypper addrepo https://download.opensuse.org/repositories/Application:Geo/15.4/Application:Geo.repo
sudo zypper addrepo https://download.opensuse.org/repositories/M17N:l10n.opensuse.org:Backports-2022.11/15.4/M17N:l10n.opensuse.org:Backports-2022.11.repo
sudo zypper refresh
sudo zypper install gdal
sudo zypper install mapserver
sudo zypper install libxml2-tools
```

For other OpenSUSE like distributions use [software.opensuse.org](https://software.opensuse.org/)
to find installation instructions for suited packages.

## Configuration

See [Configuration](./docs/configuration.md) for details.

## Usage

```shell
bin/dataloader --help
Usage:
dataloader cron                                - for starting the cron to update dops
dataloader loadmetadatafile <MetadataFileName> - to insert or update a single metadata-file into database
dataloader initpyramid                         - to initialize a new Image-Pyramid

Option:
-p propertiesFile | --properties=propertiesFile - to use a different Properties file
-l level | --loglevel=level                     - set log level
-V | --version                                  - show version information
```

If no `propertiesFile` is given `default.properties` is used.

## License

&copy; 2023 by Intevation GmbH.  
This is Free Software cover by the terms of the [GPLv2](./LICENSES/GPL-2.0-only.txt).
