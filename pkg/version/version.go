// SPDX-FileCopyrightText: 2023 Intevation GmbH <support@intevation.de>
//
// SPDX-License-Identifier: GPL-2.0-only

// Package log implements a simple leveled logging library
// on top of the log package from the standard library.

// Package version holds the version information.
package version

// SemVersion the version in semver.org format, MUST be overwritten during
// the linking stage of the build process.
var SemVersion = "2.0.3"
