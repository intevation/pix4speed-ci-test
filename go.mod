// SPDX-FileCopyrightText: 2023 Intevation GmbH <support@intevation.de>
//
// SPDX-License-Identifier: GPL-2.0-only

module heptapod.host/intevation/pix4speed

go 1.19

require (
	github.com/BurntSushi/toml v1.3.2
	github.com/google/shlex v0.0.0-20191202100458-e7afc7fbc510
	github.com/jackc/pgx/v5 v5.4.3
	github.com/jonas-p/go-shp v0.1.1
	golang.org/x/net v0.15.0
)

require (
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	golang.org/x/crypto v0.13.0 // indirect
	golang.org/x/text v0.13.0 // indirect
)
